# Function constructor. this как контекст выполнения функции. this стрелочных функций. call, apply, bind..

reduce - https://medium.com/swlh/javascript-reduce-with-examples-570f1b51e854

this — это ссылка на некий объект, к свойствам которого можно достучаться внутри вызова функции. Этот this — и есть контекст выполнения.

Но чтобы лучше понять, что такое this и контекст выполнения в JavaScript, нам потребуется зайти издалека.

Сперва вспомним, как мы в принципе можем выполнить какую-то инструкцию в коде.

Выполнить что-то в JS можно 4 способами:

Вызвав функцию;
Вызвав метод объекта;
Использовав функцию-конструктор;
Непрямым вызовом функции.

Непрямой вызов
Секция статьи "Непрямой вызов"
Непрямым вызовом называют вызов функций через .call() или .apply().

Оба первым аргументом принимают this. То есть они позволяют настроить контекст снаружи, к тому же — явно.

Связывание функций
Секция статьи "Связывание функций"
Особняком стоит .bind(). Это метод, который позволяет связывать контекст выполнения с функцией, чтобы «заранее и точно» определить, какое именно значение будет у this.

Обратите внимание, что .bind() в отличие от .call() и .apply() не вызывает функцию сразу. Вместо этого он возвращает другую функцию — связанную с указанным контекстом навсегда. Контекст у этой функции изменить невозможно.


Конструктор — это функция, которую мы используем, чтобы создавать однотипные объекты. Такие функции похожи на печатный станок, который создаёт детали LEGO. однотипные объекты — детальки, а конструктор — станок. Он как бы конструирует эти объекты, отсюда название.

По соглашениям конструкторы вызывают с помощью ключевого слова new, а также называют с большой буквы, причём обычно не глаголом, а существительным. Существительное — это та сущность, которую создаёт конструктор.

Например, если конструктор будет создавать объекты пользователей, мы можем назвать его User, а использовать вот так:

function User() {
  this.name = "Alex"
}

const firstUser = new User()
firstUser.name === "Alex"