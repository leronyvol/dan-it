const user = {
  name: 'Valerii',
  role: 'manager',
  skills: ['react', 'html', 'css'],
  say: function (a, b, c) {
    console.log(this.name, this.role, this.skills);
    console.log(this)
    console.log(a, b, c)
  },
  nested: {
    name: 'Ivan',
    role: 'user',
    skills: ['html', 'css'],
    say: () => {
      console.log(this.name, this.role, this.skills);
      console.log(this)
      // console.log(args)
    },
  }
}

const user2 = {
  name: 'User2 name',
  role: 'User2 role',
  skills: [],
}

user.nested.say();

// user.say.call(user2, 'Hello', 1, true)
// user.say('Hello', 1 , true)
// user.say.apply(user2, ['Hello', 1, true])

// user.say.bind(user2, 'test', true)(22)
// console.log(fn)
// fn(22)

// new Date()

function User(name, role) {
  // this = {} | const obj = {}
  this.name = name;
  this.role = role;

  this.connect = function () { }
  this.disconnect = function () { }

  // return 'test' | ignore
  // return { name: name + "_TEST"}
  return this
}

const createdUser = new User('Valerii', 'user')
// const createdUser2 = new User('Valerii2', 'user2')

// console.log(createdUser)

function createUser(name, role) {
  if (role === 'manager') {
    const objManager = {
      name: name,
      role: role,

      doSomething: function () { }
    }

    return objManager;
  }

  const obj = {
    name: name,
    role: role,
  }

  return obj;
}