

const promise = new Promise(function (resolve, reject) {
  setTimeout(() => {
    resolve("Kyiv");
    // reject(new Error("error"));
  }, 3000);
});
console.log(promise)

// promise
//   .then(
//     (result) => {
//       console.log('then[1]:', result);
//       return 'city '+ result
//     },
//     // (err) => console.error(err)
//   )
//   .then((data) => {
//     console.log('then[2]:', data);
//     console.log(data)
//     throw new Error('Error in then')
//   })
//   .then(data => {
//     console.log('then[3]:', data);
//   })
//   .catch((err) => console.log("from catch", err))
//   .finally(() => console.log("DONE"));

function makeOrder() {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      console.log("Order in progress");
      resolve();
    }, 500);
  });
}

function createMakeline() {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      console.log("Makeline");
      resolve();
    }, 300);
  });
}

function inOven() {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      console.log("In oven");
      if (Date.now() % 2) {
        resolve();
      } else {
        reject();
      }
    }, 400);
  });
}

function inBox() {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      console.log("In the box");
      resolve();
    }, 10);
  });
}

function onTheWay() {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      console.log("On the way");
      resolve();
    }, 0);
  });
}

// console.log("i want pizza!!!");

// makeOrder(
//     () => createMakeline(
//       () => inOven(
//         () => inBox(
//           () => onWay(
//             () => console.log("din don!")
//           )
//         )
//       )
//     )
// );

makeOrder()
  .then(() => createMakeline())
  .then(() => inOven())
  .then(() => inBox())
  .then(() => onTheWay())
  .then(() => {
    console.log("din din")
    throw new Error()
  })
  .catch(() => new Promise(function (resolve, reject){
    setTimeout(() =>{
      resolve();
    }, 300)
  }))
  .then(() => new Promise(function (resolve, reject){
    setTimeout(() =>{
      reject(new Error('Inner catch'))
    })
  }))
  .finally(() => console.log("good job!"))
  .catch(err => console.log('catch2: ', err))
  
  
