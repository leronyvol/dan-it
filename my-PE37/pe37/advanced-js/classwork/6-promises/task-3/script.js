/**
 * Написати функцію getWeather, яка приймає ім'я міста та
 * повертає проміс, який через 1 секунду зарезолвиться,
 * якщо ми передали Kyiv, Lviv, Kharkiv,
 * інакше буде помилка з текстом 'Невідоме місто'
 *
 */

const city = ['Kyiv', 'Lviv', 'Kharkiv'];

const getWeather = (cityName) => {
     return new Promise(function (resolve, reject) {
        const cityFinded = city.includes(cityName);
        if (cityFinded) {
            resolve(cityName)
        } 
        else {
            reject(new Error('error'))
        }
    })
}

getWeather('london')
    .then((data) => console.log(data))
    .catch((err) => console.error('Невідоме місто'))