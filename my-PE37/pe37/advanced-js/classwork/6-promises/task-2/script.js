/**
 * Доповнити функцію authenticate, яка буде резолвити успішно
 * проміс, якщо передали username: 'admin', password: '123', та
 * повертати помилку 'Invalid data' в іншому випадку
 *
 */

const userDb = [{userName: 'Max', password: '123345'}, {userName: 'Admin', password: '123'}];

const authenticate = (username, password) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const findedUser = userDb.find((currentUser)=>{
        return currentUser.userName === username
      
      })
      if(!findedUser) {
        reject(new Error('User not found'))
      }
      if(findedUser.password === password) {
        resolve(findedUser)
      } else {
        reject (new Error('Password incorrect'))
      }

    }, 2000);
  });
};

authenticate('Max', '123345')
.then((data)=> console.log(data))
.catch((error)=> console.log('Invalid data'))