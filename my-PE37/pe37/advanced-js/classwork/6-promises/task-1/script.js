/**
 * productsPromise робить запит на сервер і іноді з'являється помилка
 *
 * Якщо продукти приходять, то вивести їх списком на сторінку,
 * інакше вивести модальне вікно з ім'ям помилки (alert)
 *
 */

const productsPromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    if (Math.random() < 0.5) {
      resolve(["orange", "apple", "pineapple", "melon"]);
    } else {
      reject(new Error("Server Error"));
    }
  }, 2000);
})


productsPromise
  .then((result) => {
    if (Array.isArray(result)) {
      const listTemplate = result.reduce((acc, el, index) => {
        if (index === 0) acc += '<ul>'
        acc += `<li> ${el}</li>`
        if (index === result.length - 1) acc += '</ul>'
        return acc
      }, '')
      return document.querySelector('.loader').innerHTML = listTemplate
    }
  })
  .catch(err => alert(err.name))
