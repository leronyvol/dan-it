/**
 * Створити лічильники кількості населення для 3х
 * країн: України, Німеччини та Польщі
 * Для всіх країн населення спочатку дорівнює 0
 *
 * Збільшити населення на 1_000_000 для
 * України - двічі
 * Німеччини - чотири рази
 * Польщі - один раз
 *
 * При кожному збільшенні виводити в консоль
 * 'Населення COUNTRY_NAME збільшилося на X і становить Y'
 *
 * ADVANCED:
 * - Кожну секунду збільшувати населення України на 100_000 та також
 * виводити в консоль 'Населення COUNTRY_NAME збільшилося на X і становить Y'
 *
 */


function makeCountryCounter(countryName) {
    let count = 0;
    function setPopulation(value) {
        count += value;
            
        console.log(`Населення ${countryName} збільшилося на ${value} і становить ${count}`);
    }

    return setPopulation;
}

const countUkraine = makeCountryCounter('Ukraine');
countUkraine(50000);
countUkraine(420000);
const countGermany = makeCountryCounter('Germany');
countGermany(376746);
countGermany(348537684);
countGermany(46754);
const counPoland = makeCountryCounter('Poland');
counPoland(48364);
