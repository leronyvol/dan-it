{
  let username = 'firstname'

  {
    let username = 'secondname'
    function Fn() { }
  }

  [1, 2, 3].forEach(item => {
    let username = 'test'
  })

  function fn() {
    const username = 'inner name'

    function innerFn() {
      console.log(username)
    }

    return innerFn
  }

  function plus(x){
    // let x = 2

    function sum(y){
      return  x + y
    }

    return sum
  }

  const plusTwo = plus(2)
  const result =  plusTwo(10)

  // (context) => (fn) => fn.call(context)

  // function bind (context){
  //   return function(fn){
  //     fn.call(context)
  //   }
  // }

// let count1 = 0;
// let count2 = 0;

// function counter1(){
//   return ++count1;
// }

// function counter2(){
//   return ++count2;
// }



function createCounter(counter){
  function increment(){
    return ++counter
  }

  return increment
}

const counter1 = createCounter(0)
const counter2 = createCounter(0)

console.log(counter1())
console.log(counter2())
console.log(counter2())
console.log(counter2())
console.log(counter1())

}