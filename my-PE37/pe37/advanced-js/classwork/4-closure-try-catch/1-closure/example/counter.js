let counter1 = 0;
let counter2 = 0;

function increase1() {
    return ++counter1;
}

function increase2() {
    return ++counter2;
}

function makeCounter() {
    let counter = 0;
    return function () {
        return ++counter;
    }
}

const increaseCounter1 = makeCounter();
const increaseCounter2 = makeCounter();

increaseCounter1();
increaseCounter1();
increaseCounter1();
increaseCounter1();

console.log(increaseCounter2()); // 1

console.log(increaseCounter1()); // 5

// increase1();
// increase1();
// console.log(counter1); // 2

// console.log(increase1()); // 2

// increase2();
// increase1();

// console.log(increase1()); // 4

// console.log(increase1()); // 3

// console.log(increase2()); // 1