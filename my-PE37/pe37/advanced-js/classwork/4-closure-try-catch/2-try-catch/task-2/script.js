/**
 * Даний скрипт не виконується повністю, тому що виникає помилка
 *
 * Виводити помилку в консоль без блокування всього скрипта
 *
 */

const button = document.querySelector("button");

button.addEventListener("click", function () {
  console.log("button is clicked");
});

document.body.style.backgroundColor = "green";
console.log("script is loaded");
