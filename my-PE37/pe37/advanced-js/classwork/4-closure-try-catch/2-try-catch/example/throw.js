function validateUser(name) {
    if (typeof name === 'undefined') {
        const error = new Error('no name');
        throw error;
    } else if (name.length < 3) {
        throw new Error('invalid name');
    }
    return true;
}


try {
    validateUser();
    console.log('continue try');
} catch (err) {
    console.log('from catch ->');
    console.error(err);
}

console.log('testing...');
