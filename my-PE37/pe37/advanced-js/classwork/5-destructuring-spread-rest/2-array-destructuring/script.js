const cities = [["Kyiv", "Lviv"], ["Odesa", "Kharkiv"], ["Dnipro"]];

// const kyiv = cities[0];
// const lviv = cities[1];
// const odesa = cities[2];

const [[Kyiv, Lviv], arr] = cities;

// console.log(kyiv);
// console.log(lviv);
// console.log(Lviv, Kyiv, arr);

const list = [
  {
    name: "User",
  },
  {
    name: "Admin",
  },
];

const [{ name: userName }, admin] = list;
// const { name: userName } = user;

console.log(userName);
console.log(admin);


let first = 1;
let second = 2;

[second, first] = [first, second];

// console.log(first,second)
