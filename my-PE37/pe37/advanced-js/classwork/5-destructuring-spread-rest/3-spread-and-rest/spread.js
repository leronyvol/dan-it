const person = {
  name: "John",
  test: true,
};

const city = {
  city: "Kyiv",
  country: "Ukraine",
  test: false,
};

const user = {
  ...person,
  ...city,
};

const user2 = { ...user };

const obj = Object.assign({}, person, city);

console.log("user 1:", user);
user.test = "banana";
console.log("user 2:", user2);
console.log(user === user2);

const cities = ["Kyiv", "Lviv", "Odesa", "Kharkiv", "Dnipro"];

const [kyiv, lviv, ...other] = cities;

console.log(kyiv);
console.log(lviv);
console.log(other);

const fn = function (a, b, ...args) {
  console.log("fn:", a, b);
  console.log(args);
};

const fnArrow = ({ name, id }, b, ...argsArrow) => {
  //   const { name, id } = a;
  console.log("arrowFn:", name, id);
  console.log(argsArrow);
};

fnArrow({ name: "Test user", id: 987654321 }, 2, 3, true, "string", [], {});

// const ReactCompo = ({ name, test}) => (<div>Test</div>)
