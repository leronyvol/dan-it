// rest in object objects
// rest in arrays

// difference in assigning primitive and reference Types

// clone objects 
// clone arrays

// merge objects
// merge arrays


let a = {
    num: 10
}

let b = { 
    num: -30,
    str: 'hello'
 };

 let c = {...a, ...b, test: true, num: 15};

 const test = Object.assign({}, a, b, { test: true, num: 15})

console.log(test)

// b.num = 30;
// a.num = 50;
// a.test = 'test';

// console.log('a -> ');
// console.log(a); // { num: 10 }
// console.log('b -> ');
// console.log(b); // { num: 10 }

// const tetsFn = function(a, ...rest){
//     console.log(a, rest)
// }

// tetsFn(1, 2, 3, 'five')

const arr = [1, 2, 3, 4]

const [firstEl, secondEl, ...restArr] = arr

console.log(firstEl, secondEl, restArr)