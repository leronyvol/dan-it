// користувач намагався заповнити 3 рази форму на сайті
// при тому деякі поля він пропускав, а деякі заповнював по новій
// вся інформація відповідно знаходиться в трьох різних об'єктах
//
// необхідно її об'єднати в один об'єкт з назвою user та
// результат вивести в консоль

const form1 = {
  name: "Ricky Torres",
  nickname: "ricky23",
  country: "United States",
};

const form2 = {
  name: "Ricky",
  surname: "Torres",
  sex: "male",
};

const form3 = {
  age: 23,
  nickname: "ricky_torres23",
  status: "married",
};

const validForm = {...form1, ...form2, ...form3};

console.log(validForm);