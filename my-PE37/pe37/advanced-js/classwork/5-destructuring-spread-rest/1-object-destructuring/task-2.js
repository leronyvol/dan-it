// - створити об'єкт вакансії для фронтенду (frontendVacancy) та бекенду (backendVacancy), які будуть мати наступний вигляд
// {
//    company: 'DAN.IT',
//    city: 'Kyiv',
//    skills: [...]
// }
// - вивести кожну вакансію в консоль

const geoLocation = {
  name: "Kyiv",
  type: "city",
  country: "Ukraine",
};

const company = {
  title: "DAN.IT",
  employeesCount: 32,
  type: "private",
};

const skills = {
  frontend: [
    {
      name: "JS",
      experience: 3,
    },
    {
      name: "React + Redux",
      experience: 2,
    },
    {
      name: "HTML5",
      experience: 2,
    },
    {
      name: "CSS3",
      experience: 2,
    },
  ],
  backend: [
    {
      name: "PHP",
      experience: 2,
    },
  ],
};

const {name:city} = geoLocation;
const{title} = company;
const {frontend, backend} = skills;

const frontendVacancy = {
  city,
  company:title,
  skills:frontend
};
const backendVacancy = {
  city,
  company:title,
  skills:backend
};

console.log(frontendVacancy);
console.log(backendVacancy);