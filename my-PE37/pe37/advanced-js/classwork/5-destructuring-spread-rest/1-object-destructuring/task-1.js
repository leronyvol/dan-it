// присвоїти в змінні назву продукту, модель та ціну та
// вивести в консоль значення кожної створеної змінної

const product = {
  name: "iPhone",
  model: "12 Pro",
  company: "Apple",
  price: 1400,
};

const {
  name,
  model,
  price,
} = product;

console.log(name, model, price);