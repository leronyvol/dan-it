// const user = {
//   firstName: "John",
//   lastName: "Mikel",
//   age: 21,
//   isAdmin: true,
// };

const person = {
  firstName: "Rachel",
  age: 23,
  isAdmin: true,
  test: null,
};

const user = {
  name: {
      first: 'John',
      last: 'Torres',
      // family: {
      //     dad: 'Jack'
      // }
  },
  age: 37
}

let {
  name: {
    last,
    family: {
      dad = 'Default value'
    } = {}
  } = {},
  age
} = user;

// name = 'new name'
// const firstName = user.firstName
name.last = 'TEST'
const  exampleDad = user && user.name && user.name.family && user.name.family.dad ? user.name.family.dad : 'Default value' 
console.log(dad)

