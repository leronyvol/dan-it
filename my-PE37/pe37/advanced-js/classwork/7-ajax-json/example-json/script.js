const person = {
    name: 'John',
    age: 24,
    lang: ['php', 'js']
}

const personStr = JSON.stringify(person);
const personObj = JSON.parse(personStr)

console.log(personStr);
console.log(personObj.name);
