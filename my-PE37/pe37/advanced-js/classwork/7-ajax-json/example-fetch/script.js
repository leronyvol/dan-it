const fetchResult = fetch("https://dog.ceo/api/breeds/image/random")

fetchResult
.then(rsp => rsp.json())
.then(({message: src}) => {
  console.log(src)
  document.body.insertAdjacentHTML("beforeend", `<img style="width: 100%" src="${src}">`);
})


// fetchResult
//   .then(
//     (response) => response.json(), 
//   )
//   .then((data) => {
//     console.log('data', data)
//     data.forEach((post) => {
//       console.log('post', post)
//       document.body.insertAdjacentHTML("beforeend", `<p>${post.title}</p>`);
//     });
//   });
