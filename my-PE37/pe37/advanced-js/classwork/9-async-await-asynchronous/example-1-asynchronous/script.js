function fetchFilms() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.warn('fetch films')
      resolve([
        { id: 1, name: "Blade Runner", ratin: 8.1 },
        { id: 2, name: "Dark City", ratin: 7.6 },
      ]);
    }, 2000);
  });
}

function fetchActors(filmName) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('fetch actors')
      switch(filmName) {
        case "Blade Runner":
          resolve(['Harrison Ford', 'Rutger Hauer', 'Sean Young'])
          break;
        case "Dark City":
          resolve(['Rufus Sewell', 'William Hurt', 'Jennifer Connelly']);
          break;
        default:
          reject(new Error('Invalid film name'));
      }
    })
  })
}

const renderActorByFilm = async () => {
  console.log('SHOW SPINNER')

  try{
    const films = await fetchFilms()

    // for (let film of films ){
    //   const actors = await fetchActors(film.name)
    //   console.log(film.name, actors)
    // }

    films.forEach(async (film) => {
      const actors = await fetchActors(film.name)

      console.log(film.name, actors)
    })
  }catch(err){
     alert('Failed to fetch film', err.message)
  }
  finally{
    console.log('HIDE SPINNER')
  }
  
}

renderActorByFilm()

// const asyncFn = async () => {
//   // return 1;
//   throw new Error('ooops')
// };

// const asyncFn = new Promise((resolve) => {
//     // resolve(1);
//     reject(new Error('ooops'));
// })

// console.log(asyncFn());

// asyncFn()
// .then( data => console.log(data))
// .catch(err => console.warn(err))
