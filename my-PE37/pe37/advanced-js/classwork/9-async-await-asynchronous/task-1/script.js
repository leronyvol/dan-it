const BASE_URL = 'https://ajax.test-danit.com/api/json'

const fetchPosts = async function () {
    try {
        const response = await fetch(`${BASE_URL}/posts`)
        if(response.status >= 200 && response.status < 300){
            const data = await response.json()
            return data
        } 
        throw new Error(response.status)
    } catch (error) {
        throw new Error(error)
    }
}
const fetchCommentsById = async function (id) {
    try {
        const response = await fetch(`${BASE_URL}/comments?postId=${id}`)
        if(response.status >= 200 && response.status < 300){
            const data = await response.json()
            return data
        } 
        throw new Error(response.status)
    } catch (error) {
        throw new Error(error)
    }
}

function renderPostIdSelector (selectorId, idList) {
    const selectorEl = document.getElementById(selectorId)

    if(!(selectorEl instanceof HTMLElement)) throw new Error('Slector element not found');

    selectorEl.innerHTML = '';

    idList.forEach(id => {
        selectorEl.innerHTML += `<option value="${id}">${id}</option>`
    })
}

function renderPostCommnets(id, comments){
    const commentsEl = document.getElementById(id)

    if(!(commentsEl instanceof HTMLElement)) throw new Error('Comments element not found');

    commentsEl.innerHTML = '';
    comments.forEach(({ body }, index) => {
        commentsEl.innerHTML += `<blockquote>[${index}]\n${body}</blockquote>`
    })

}

function renderPostTitle(id, title){
    const titleEl = document.getElementById(id)

    if(!typeof title === 'string') throw new Error('Title must be a string')

    if(!(titleEl instanceof HTMLElement)) throw new Error('Title element not found')

    titleEl.innerHTML = title
}


async function initApp (){
    try {
        const posts = await fetchPosts()
        const postsId = posts.map(post => post.id)

        renderPostIdSelector('post', postsId)

        document.getElementById('post')
        .addEventListener('change', async (e) => {
            const selectedId = e.target.value;
            const selectedPost = posts.find(post => post.id === parseInt(selectedId, 10))
            const title = !!selectedPost ? selectedPost.title : ''

            try{
                const comments = await fetchCommentsById(selectedId)
                renderPostCommnets('comments', comments);
                renderPostTitle('title', title)
            }catch(err){
                throw new Error(err)
            }
        })
    }
    catch {}
}

initApp()