// ## Технічні вимоги
// При створенні задається виробник, модель та ціна 

// Метод getName() повинен повертати повну назву продукту:
// ВИРОБНИК + МОДЕЛЬ, наприклад Apple Macbook Pro 13"

// Для телефонів також задається наявність NFC чипу,
// а для планшетів наявність слоту для sim карти

// Кожен продукт має націнку 20%, додати метод, який
// буде повертати ціну вже з націнкою

// ### Розрахунок вартості доставки
// Кожен тип продукту (ноутбук, планшет та телефон) має свою логіку розрахунку вартості доставки:

// #### Для НОУТБУКУ
//  * якщо ціна з націнкою більше 30 000, то доставка безкоштовна
//  * інакше доставка коштує 200

// #### Для ПЛАНШЕТУ
//  * якщо ціна з націнкою більше 20 000, то доставка безкоштовна
//  * інакше ціна доставки це 1% від ціни з націнкою

// #### Для ТЕЛЕФОНУ
//  * якщо ціна з націнкою більше 10 000, то доставка розраховується як 1% від загальної ціни
//  * інакше береться 3%

class Devices {
    constructor(creator, model, price) {
        this.creator = creator;
        this.model = model;
        this._price = price;
    }
    getName() {
        this.creator + this.model
    }

    get price() {
        return this._price * 1.2;
    }
    set price(value) {
        console.log('Price read only')
        this._price = value;
    }

}
class Mobile extends Devices {
    constructor(creator, model, price, nfc) {
        super(creator, model, price);
        this.nfc = nfc;
    }
    getShipping() {
        return this.price > 10000 ? this.price * 0.01 : this.price * 0.03;
    }
}
class Tablet extends Devices {
    constructor(creator, model, price, sim) {
        super(creator, model, price);
        this.sim = sim;
    }
    getShipping() {
        return this.price > 20000 ? 0 : this.price * 0.01;
    }
}
class Laptop extends Devices {
    getShipping() {
        return this.price > 30000 ? 0 : 200;
    }
}
class PremMobile extends Mobile{
    get price() {
        return this._price * 1.35;
    }
}

const telephone = new Mobile("Samsung", "S9", 20000, true);
const tablet = new Tablet("Lenovo", "New", 20000, true);
const laptop = new Laptop("MSI", "Smart", 40000);
const huawei = new PremMobile("Huawei", "PSmart", 20000, true);
console.log(telephone);
console.log(telephone.price);
console.log(telephone.getShipping());
console.log(huawei);
console.log(huawei.price);
console.log(huawei.getShipping());


