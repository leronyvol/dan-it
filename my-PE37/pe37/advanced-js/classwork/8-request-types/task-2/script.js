function post(url, body, queryParams) {
  const queryParamsString = new URLSearchParams(queryParams);

  return fetch(`${url} ${!!queryParams ? "?" + queryParamsString : ""}`, {
    method: "POST",
    body: JSON.stringify(body),
  }).then((resp) => {
    if (resp.ok) {
      return resp.json();
    } else {
      return new Error("fetch oops... i did it again!!!");
    }
  });
}

function createUser(name, email, website) {
  const baseUrl = "https://ajax.test-danit.com/api/json";
  return post(`${baseUrl}/users`, { name, email, website });
}

const searchButton = document.querySelector("button[type=submit]");

searchButton.addEventListener("click", (event) => {
  event.preventDefault();
  let name = document.getElementById("name").value;
  let email = document.getElementById("email").value;
  let website = document.getElementById("website").value;

  createUser(name, email, website)
    .then((data) => alert(JSON.stringify(data)))
    .catch((err) => console.error(err));
});
