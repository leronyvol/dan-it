const BASE_API_URL = "http://localhost:8081";

// ### GET

fetch(`${BASE_API_URL}/users/9`)
  .then((response) => response.json())
  .then((json) => console.log(json));

// fetch(`${BASE_API_URL}/users`, {
//   method: "GET",
// })
//   .then((response) => response.json())
//   .then((json) => console.log(json));

// query params - MAX 2,048 characters

// fetch(`${BASE_API_URL}/users?role=admin`, {
//   method: "GET",
// })
//   .then((response) => response.json())
//   .then((json) => console.log("with query params", json));

// const queryParams = new URLSearchParams({
//   role: "user",
//   is_active: true,
// });

// fetch(`${BASE_API_URL}/users?${queryParams}`, {
//   method: "GET",
// })
//   .then((response) => response.json())
//   .then((json) => console.log("with query params", json));

// ### POST

// fetch(`${BASE_API_URL}/users`, {
//   method: "POST",
//   headers: {
//     "Content-Type": "application/json",
//   },
//   body: JSON.stringify({
//     name: "Valerii M",
//     role: "trainer",
//     email: "valerii.m@example.com",
//     is_active: false,
//   }),
// })
//   .then((response) => response.json())
//   .then((json) => console.log(json))
//   .then(() => {
//       debugger
//   })

// ### PUT

// fetch(`${BASE_API_URL}/users/0`, {
//   method: "PUT",
//   headers: {
//     "Content-Type": "application/json",
//   },
//   body: JSON.stringify({
//     email: "debra.gibson@example.com",
//     role: "user",
//     is_active: true,
//     name: "Debra Gibson",
//   }),
// })
//   .then((response) => response.json())
//   .then((json) => console.log(json));

// ### DELETE

// fetch(`${BASE_API_URL}/users/0`, {
//   method: "DELETE",
// })
//   .then((response) => response.json())
//   .then((json) => console.log(json));
