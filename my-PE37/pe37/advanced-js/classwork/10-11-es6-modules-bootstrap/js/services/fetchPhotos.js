const PHOTO_LIMIT = 9
const BASE_URL = 'https://picsum.photos'
const fetchPhotosByPageId = async function(pageNum){
     try {const response = await fetch(`${BASE_URL}/v2/list?page=${pageNum}&limit=${PHOTO_LIMIT}`)
    if (!response.ok) throw new Error(response.status)
    return response.json()}
    catch(e){
        throw new Error(e)
    }
}
export default fetchPhotosByPageId