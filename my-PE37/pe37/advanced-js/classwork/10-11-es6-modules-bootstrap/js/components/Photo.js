class Photo {
    constructor(src, height, width) {
      this.src = src;
      this.height = height;
      this.width = width;
    }
  getTemplate() {
    return `<img src="${this.src}" class="img-fluid" alt="..." width='${this.width}' height='${this.height}'>`
  } 
  updateProps({src, height, width}) {
    this.src = src;
    this.height = height;
    this.width = width;
  }  
  render(id) {
    const container = document.getElementById(id)
    if(!(container instanceof HTMLElement)) throw new Error('Absent DOM Element')
    container.insertAdjacentHTML("afterbegin", this.getTemplate());
  }
  }
  
  export default Photo