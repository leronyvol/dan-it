/* TASK - 3
 * Get the access group name from the user, it can be - 'admin', 'manager' or 'user'.
 * And show different message for different access group:
 *   - for admin - "Hello, admin!"
 *   - for manager - "Hello, manager!"
 *   - for user - "Hello, user!"
 *   */
let groupName = prompt('Who are you? Admin, manager or user?');

if ((isNaN(groupName) ||  groupName === '' || groupName === false)) {
    switch (groupName) {
        case 'admin':
            alert('Hello, admin!');
            let admin = prompt('Are you a developer or a designer?');
			switch (admin) {
				case 'developer':
					window.location.assign(
						'https://www.devteam.space/wp-content/uploads/2019/11/Getting-Rid-Of-Bad-Developers-During-A-Project.gif'
					);
					break;
				case 'designer':
					window.location.assign('https://www.figma.com/resources/learn-design/');
					break;
				default:
					alert('You entered something wrong!');
			}
			break;
		case 'manager':
			alert('Hello, manager!');
			break;
		case 'user':
			alert('Hello, user!');
			break;
		default:
			alert('Sorry, try again');
	}
} else {
	alert('Sorry, enter your access group name, please!');
}
