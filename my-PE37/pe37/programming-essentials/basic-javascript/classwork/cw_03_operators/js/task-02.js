/* TASK - 2
 * Ask user in witch language he wants to see the list of the days.
 * User should enter one of three values, they are - urk, en, ru
 * Show the list of days on selected language.
 * */

let langen = 'Saturday';
let langru = 'Суббота';
let langukr = 'Субота';

let userLanguage = prompt('Введите предпочитаемый язык', '2');

if (isNaN(userLanguage)) {
	switch (userLanguage) {
		case 'ukr':
			document.write(`<h1>${langukr}</h1>`);
			break;

		case 'en':
			document.write(langen);
			break;

		case 'ru':
			document.write(langru);
			break;
		default:
			document.write('Не верный ввод. Повторите.');
	}
} else {
	document.write('Не верный ввод. Повторите.');
}
