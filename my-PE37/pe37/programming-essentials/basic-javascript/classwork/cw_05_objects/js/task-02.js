/* ЗАДАНИЕ - 2
 * Добавить к предыдущему заданию функционал.
 * В возвращаемом объекте должен быть метод, который увеличивает возраст на 1.
 * Т.е. внутри объекта будет свойство\ключ\поле, значением которого будет являться функция,
 * которая увеличивает свойство\ключ\поле age ЭТОГО объекта на 1
 * */

// let userPromptName = prompt('what u name')
// let userPromptAge = +prompt('what u age')

function getPerson(name, age) {
	let person = {
		name,
		age,
		incrementAge: function () {
			// person.age++
			person.age += 5
		},
	}
    return person
}

const result = getPerson('andrey', 26)
console.log(result)

result.incrementAge()
console.log(result)
