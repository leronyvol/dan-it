/* ЗАДАНИЕ - 3
 * Добавить к предыдущему заданию функционал.
 * В возвращаемом объекте должен появиться еще один метод - addField(). Он будет добавлять свойства в объект.
 * Т.е. внутри объекта будет еще одно свойство\ключ\поле, значением которого будет являться функция.
 * Эта функция принимает два аргумента:
 *   1 - имя свойства, которое будет создаваться
 *   2 - значение. которое туда должно быть записано
 * */

function getPerson(name, age) {
	let person = {
		name,
		age,
		incrementAge: function () {
			// person.age++
			person.age += 5
		},
		addField: function (keyObj, value) {
			person[keyObj] = value
		},
	}
	return person
}




const result = getPerson('andrey', 26)

// result['hobby'] = 'baskets'
console.log(result)


// console.log(result)
debugger
result.addField('hobby', 'cybersport')
result.addField('condition', true)
console.log(result)
