/**
 * Task - 02
 * Create a <p> element that should represent how many pixels the user had scrolled on the page from the top of it.
 * */

const p = document.createElement('p')
document.body.prepend(p)
p.style.cssText =
	'position : fixed; top : 20px; left : 20px; font-size: 30px; background-color: green;'
window.addEventListener('scroll', function () {
	p.textContent = parseInt(this.pageYOffset) + 'px'
})
