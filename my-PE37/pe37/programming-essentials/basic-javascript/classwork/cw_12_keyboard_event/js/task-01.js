/**
 * Task - 01
 *
 * Create h1 element on a page using JavaScript. Place it on the page.
 * After any key on the page was pressed, print the key's name as the text content of the h1 element.
 * */

const h1 = document.createElement('h1')
document.body.prepend(h1)

window.onkeyup =  function (e) {
	h1.textContent += e.key;
	// h1.textContent = h1.textContent + e.key
    console.log(e.target);
}
