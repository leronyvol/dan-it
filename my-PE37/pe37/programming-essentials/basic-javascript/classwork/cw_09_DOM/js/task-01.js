/* TASK - 1
 * Get several elements from the page, by:
 *   tag
 *   class
 *   identifier
 *   CSS selector
 *   name attribute
 * Use console.dir() method to show up the elements
 * */

let tag = document.getElementsByTagName('h1')

let getClassElem = document.getElementsByClassName('title')

let identifier = document.getElementById('root')

let selector = document.querySelector('.title')

let attribute = document.querySelector('[name = "hello"]')

console.log("🚀 ==== > псевдо getClassElem", getClassElem);
console.log('🚀 ==== > псевдо tag', tag[0])



console.log('🚀 ==== > явный identifier', identifier)
console.log("🚀 ==== > явный selector", selector);
console.log("🚀 ==== > явный attribute", attribute);
