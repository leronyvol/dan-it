/* TASK - 2
 * Create a function that will take one argument - array from the previous task result.
 * The task is to show every dish from the array in the console and delete it from the source array.
 * After all of the dishes will be shown on the console, the array must be empty.
 * */

const getBreakfastUser = () => {
	let breakfast = []
	let dish = prompt('Что будете на завтрак?')

	while (dish !== 'end') {
		breakfast.push(dish)
		dish = prompt('Что будете на завтрак?')
	}

	return breakfast
}

function cleanArray(srcArray) {
	const length = srcArray.length
    
	for (let i = 0; i < length; i++) {
		console.log(srcArray.pop())
	}

	console.log(srcArray)
}

cleanArray(getBreakfastUser())
