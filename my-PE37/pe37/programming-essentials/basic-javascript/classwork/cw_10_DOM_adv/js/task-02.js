/* TASK - 2
 * Create two squares, using the same way that described in the previous task.
 * But these squares are going to have different background colors.
 * Create two elements in JS
 * Ask the user about the size for both of these squares
 * Ask the user about the fist background-color
 * Ask the user about the second background-color
 * Add the styles to both squares
 * Place both squares BEFORE the first element with the script tag on the page
 */

const createSquare = (size, color) => {
	let element = document.createElement('div')
	element.style.width = size + 'px'
	element.style.height = size + 'px'
	element.style.backgroundColor = color
	element.style.margin = '10px'
	return element
}

function createMoreSquare(count, parent) {
	let sizeUsers = parseInt(prompt('Введите размер стороны'))

	for (let i = 0; i < count; i++) {
		const generateColor = '#' + Math.random().toString(16).substr(-6)
		let square = createSquare(sizeUsers, generateColor) // <div style={width: size}></div>
		parent.before(square)
	}
}

let someScript = document.querySelector('script')
createMoreSquare(10, someScript) // first params "count" = 10; second params "parent" = someScript
