/*TASK - 3
 * Create a function, that will be changing the background color of the 100px square randomly,
 * by the clicking on it. Every color should be random, transparent and white are not included in the list of colors.
 */

function createSquare(size) {
	const block = document.createElement('div')
	block.style.width = size + 'px'
	block.style.height = size + 'px'
	block.style.backgroundColor = '#000'
	return block
}

const square = createSquare(100)
document.body.prepend(square)

// const generateColor = params => {
// 	const randomNum = Math.random() * params
// 	let result = Math.floor(randomNum)
// 	return result
// }
const generateColor = params => Math.floor(Math.random() * params)

square.addEventListener('click', function (event) {
	event.preventDefault()
	let changeColor = `rgb(${generateColor(255)},${generateColor(200)},${generateColor(255)})`
	this.style.backgroundColor = changeColor
})
