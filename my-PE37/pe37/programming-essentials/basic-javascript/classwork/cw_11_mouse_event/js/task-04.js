/*TASK - 4
 * There is a button 'Change color' next ot the 100px square. Create a function, that will change the color of the square randomly, by the clicking on the button, not on the square.
 */

function createSquare(size) {
	// создаем функцию createSquare для того чтобы она нам отдавала готовый квадрват с размерым
	// который мы ей передеаем в качестве аргумента size
	const block = document.createElement('div')
	block.style.width = size + 'px'
	block.style.height = size + 'px'
	block.style.backgroundColor = '#000'
	return block
}

// Вызываем функцию createSquare записывая в переменную результат её выполнения 
const square = createSquare(100)
// Помещяем созданный див в разметку
document.body.prepend(square)

// создаем функцию для генерации случайного числа от 0 до переданого в виде аргумента params
const generateColor = params => Math.floor(Math.random() * params)

// создаем функцию createButton с 2 аргументами, который нам нужны для того чтобы при каждом вызове
// присваивать кнопке уникальный контент и слушатель событий
function createButton(text, callback){
    const btn = document.createElement('button')
    btn.innerText = text
    btn.addEventListener('click', callback) 
    return btn
}
// вызываем функцию передавая тут контент кнопки и функцию которая по итогу будет выполнена только по нажатию на кнопку
// так как она является аргументом callback
const button = createButton('change color', changeColor)
square.after(button)

const showConsole = () => {
	console.log('object')
}
// происходит похожая процедура создания еще одной кнопки и передачи ей уже других аргументов
const btn1 = createButton('что-то написано', showConsole)
document.body.prepend(btn1)

function changeColor() {
	let changeColor = `rgb(${generateColor(255)},${generateColor(200)},${generateColor(255)})`
	square.style.backgroundColor = changeColor
}

// const btn = document.createElement('button')
// btn.innerText = 'click'
// btn.addEventListener('click', () => {
// 	let changeColor = `rgb(${generateColor(255)},${generateColor(200)},${generateColor(255)})`
// 	square.style.backgroundColor = changeColor
// })
// const btn1 = document.createElement('button')
// btn1.innerText = 'hello'
// btn1.addEventListener('click', () => {
// 	alert('welcome')
// })
