/*TASK - 5
 * There is an input next to the 100px square.
 * Only HEX of the color can be entered in this input. Next to this input - 'Ok' btn is placed and its inactive by default.
 * Create a function that will be changing the color.
 * After hex is entered 'Ok' button should become active so user could press it.
 * After pressing the 'Ok' button color should changes.
 * */

function createSquare(size) {
	const block = document.createElement('div')
	block.style.width = size + 'px'
	block.style.height = size + 'px'
	block.style.backgroundColor = '#000'
	return block
}
const square = createSquare(100)
document.body.prepend(square)

const inp = document.createElement('input')
inp.type = 'color'
document.body.append(inp)

const btn = document.createElement('button')
btn.textContent = 'Ok'
// btn.disabled = 'true'
document.body.append(btn)

btn.addEventListener('click', () => {
	square.style.backgroundColor = inp.value
})
