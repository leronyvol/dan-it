/* ЗАДАНИЕ - 2
 * Вывести в консоль первые 147 непарных чисел.
 *   ПРОДВИНУТАЯ СЛОЖНОСТЬ - не выводить в консоль те числа, которые делятся на 5.
 * */
// EXAMPLE 1
// let number = 0;
// let count = 0;
// while (count < 147) {
// 	if (number % 2 !== 0 && number % 5 !== 0) {
// 			console.log(number);
// 			count++;
// 	}
// 	number++;
// }

// EXAMPLE 2
// debugger
// for (let i = 0, number = 1; number <= 147; i++) {
// 	if (i % 2 !== 0 && i % 5 !== 0) {
// 		console.log(i);
// 		number++;
// 	}
// }
for (let i = 0, tries = 1; i === 1; tries++ ) {   
    let name = prompt('Enter username');
    if (isNaN(name) && name !== null && name !==""){
        i++
        console.log(name)
    }
}
