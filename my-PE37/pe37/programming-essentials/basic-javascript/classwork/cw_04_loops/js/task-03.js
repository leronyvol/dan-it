/* ЗАДАНИЕ - 3
 * Осуществляем проверку на корректность введения данных.
 * Пользователь должен ввести два числа и операцию.
 * Если пользователь ввел НЕ числа или операцию, которой нет в списке - спрашиваем все по новой, ДО ТЕХ ПОР ПОКА не введет правильно.
 *  Список операций:
 *   * - умножение
 *   + - добавление
 *   - - вычетание
 *   / - деление
 * */

let firstNum = +prompt('enter first num')
while (isNaN(firstNum)) {
     firstNum = +prompt('enter first num')
}
let secondNum = +prompt('enter second num')
while (isNaN(secondNum)) {
	secondNum = +prompt('enter first num');
}

let operation = prompt('enter operation with numbers')

while (
	operation !== '+' &&
	operation !== '-' &&
	operation !== '/' &&
	operation !== '*'
) {
	operation = prompt('enter operation with numbers');
}