/* TASK 4
 * Create a function getDayAgo(numberOfDays)
 *
 * Return value: name of the weekday, that was numberOfDays days before.
 */
function getDayAgo(numberOfDays) {
	const today = new Date()
	const dayNow = today.getDate()

	today.setDate(dayNow - numberOfDays)

	const weekDays = {
		0: 'воскресенье',
		1: 'понедельник',
		2: 'вторник',
		3: 'среда',
		4: 'четверг',
		5: 'пятница',
		6: 'суббота',
	}
	let indexOftheDay = today.getDay()
	return weekDays[indexOftheDay]
}
debugger
let res = getDayAgo(2)
console.log('🚀 ==== > res', res)
