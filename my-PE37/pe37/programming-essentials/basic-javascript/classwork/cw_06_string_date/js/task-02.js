/*TASK 2
 * Create a function. It should receive 1 argument - source string.
 *
 * Turn all of the odd characters of the string in to UPPERCASE.
 *
 * Return value: string with uppercase odd characters
 */
const str =
	'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A architecto atque dignissimos distinctio earum est facere facilis fuga, ipsa labore laborum obcaecati possimus qui ratione similique suscipit tempora temporibus totam.'

// str.toLocaleUpperCase()

function transformOddIndex(srcStr) {
	let result = ''
	for (let i = 0; i < srcStr.length; i++) {
		if (i % 2 === 0) {
			result += srcStr[i].toUpperCase()
		} else {
			result += srcStr[i]
		}

		// i % 2 === 0 ? (result += srcStr[i].toUpperCase()) : (result += srcStr[i])
	}
	return result
}

const transformStr = transformOddIndex(str)
console.log(transformStr)
