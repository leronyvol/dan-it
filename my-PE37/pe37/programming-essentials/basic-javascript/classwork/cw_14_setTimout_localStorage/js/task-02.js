/* TASK - 2
 * Add third argument into previous task function. It will be the number which will describe how many times you want to show the message.
 * */

function showMsg(msgText, time, count) {
    
	const timer = setInterval(() => {
		console.log(msgText)
		count--
		if (count <= 0) {
			clearInterval(timer)
		}
	}, time)
}

showMsg('hello func it"s work', 1000, 4)
// function showMsg(msgText, time, number) {
// 	setTimeout(() => {
// 		if (number === 0) {
// 			return
// 		} else {
// 			console.log(msgText)
// 			number--
// 			showMsg(msgText, time, number)
// 		}
// 	}, time)
// }
showMsg('Hello', 4000, 5)
