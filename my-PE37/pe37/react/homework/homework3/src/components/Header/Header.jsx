import React from 'react';
import "./Header.scss"
import {Routes, Route, Link} from "react-router-dom"
import Fav from "../../pages/Favorite/Favorite"
import Basket from "../../pages/Basket/Basket"
import Home from "../../pages/Home/Home"

const Header = () => {
    return (
        <>
            <header className="header">
                <Link className="a" to="/">Home</Link>
                <Link className="a" to="/favorite"> Favorite⭐</Link>
                <Link className="a" to="/basket">Basket🗑</Link>
            </header>
            <Routes>
                <Route path="/" element={<Home/>}/>
                <Route path="/favorite" element={<Fav/>}/>
                <Route path="/basket" element={<Basket/>}/>
            </Routes>

        </>
    );
};

export default Header;
