import React, {useState, useEffect} from 'react';
import styles from './ProductCard.scss'
import Button from "../Button/Button"
import {getFavoriteItem} from "../../helper";

import PropTypes from 'prop-types';


const ProductCard = (props) => {
    const {
        name,
        price,
        url,
        code,
        color

    } = props.item;


    const [isFavorite, noFavorite] = useState("☆");
    useEffect(() => {
        const {filter} = getFavoriteItem(code);
        !filter ? noFavorite("☆") : noFavorite("★")
    }, []);

    const onFavoriteClick = () => {
        const nextStore = props.store.filter(item => item !== code );
        console.log(nextStore);
        props.setStore(nextStore)
    };

    const toggleFavorite = () => {
        const {favoriteArr, filter} = getFavoriteItem(code);
        filter ? noFavorite("☆") : noFavorite("★");
        const newFavorite = filter ? favoriteArr.filter(elem => elem !== code) : [...favoriteArr, code];
        localStorage.setItem("favoriteItem", JSON.stringify(newFavorite))
        if (props.store) {
            onFavoriteClick()
        }
    };
    const handelAddToCart = () =>{
        props.onClick();
        props.setProductToCart(props.item)
    }

    return (

        <div className="products">
            <h1 className="name">{name}</h1>

            <h1 className="star" onClick={toggleFavorite}>{isFavorite}</h1>

            <span className="price">Цена: {price}</span>
            <img className="image" src={url} alt=""/>
            <p className="code">{code}</p>
            <p className="color">{color}</p>
            <Button text={'Add to cart'}
                    onClick={handelAddToCart}
                    backgroundColor='red'
                    justifyContent='center'
                    padding='10px 30px'
                    border='none'
                    borderRadius='7px'
                    alignItems='center'/>
        </div>
    )
}

ProductCard.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    url: PropTypes.string,
    code: PropTypes.string,
    color: PropTypes.string,
};
ProductCard.defaultProps = {
    name: "",
    price: "",
    url: "",
    code: "",
    color: "",
}


export default ProductCard
