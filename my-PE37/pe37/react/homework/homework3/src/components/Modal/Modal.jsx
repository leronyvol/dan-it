import React from 'react';
import "./Modal.scss"
import MyButton from "../Button/Button";


const Modal = ({header, closeModal, text, firstBtn, secondBtn, active, ModalBox, headerBackground, setProductToCart, productToCart}) => {
const clickAddToCart = () => {
    const cart = localStorage.getItem("cardItems");
    const isCart = JSON.parse(cart);
    closeModal()
    isCart.push(productToCart)
    localStorage.setItem("cardItems", JSON.stringify(isCart))
}

    return (
        <div className="Modal" style={active} onClick={e => (e.currentTarget === e.target) && closeModal()}>
            <div style={ModalBox} className="ModalBox">
                <div className="ModalHeader" style={headerBackground}>
                    <h2>{header}</h2>
                    <span onClick={closeModal}>X</span>
                </div>
                <div className="text">{text}</div>
                <footer>
                    <MyButton onClick={ clickAddToCart } text={"Add to cart"}/>
                    <MyButton onClick={() => localStorage.setItem("secondBtn", secondBtn)} text={secondBtn}/>
                </footer>
            </div>
        </div>
    )
};
export default Modal;
