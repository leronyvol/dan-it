import React, {useState, useEffect} from 'react';
import ProductCard from "../ProductCard";
import "./Card.scss"

const Cards = (props) => {

    const [data, setData] = useState([]);

    useEffect(() => {
        async function fetchData() {
            const fetchData = await fetch('./Product.json');
            const data = await fetchData.json();
            setData(data)
        }
        fetchData().then()
    }, []);

    const list = data.map(item => (<ProductCard
       setProductToCart={props.setProductToCart}
        item={item}
        onClick={props.openFirst}
        key={item.code}/>));

    return (
        <div className="cards">
            {list}
        </div>
    )
};

export default Cards;
