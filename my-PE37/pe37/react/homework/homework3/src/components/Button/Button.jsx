import React from 'react';
import styles from './Button.scss'

const Button = ({backgroundColor, text, onClick}) => (
    <button className={styles.MyButton}
            style={{backgroundColor}}
            onClick={onClick}>
        {text}
    </button>
);


export default Button;