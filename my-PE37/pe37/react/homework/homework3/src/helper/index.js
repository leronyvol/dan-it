 export const getFavoriteItem = (code) => {
     const favorite = localStorage.getItem("favoriteItem");
     const favoriteArr = JSON.parse(favorite);
     const filter = favoriteArr.find(item => item === code);
     return {favoriteArr, filter}
 };