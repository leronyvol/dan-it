import React, { useState, useEffect } from 'react';
import ProductCard from '../../components/ProductCard/ProductCard'

const Fav = (props) => {
    const [store, setStore] = useState([]);
    console.log(store);

    const localStor = JSON.parse(localStorage.getItem("favoriteItem"));
    console.log("typeof",typeof localStor);
    const [data, setData] = useState([]);

    useEffect(() => {
        setStore(localStor);
        async function fetchData() {
            const fetchData = await fetch('./Product.json');
            const data = await fetchData.json();
            setData(data)
        }
        fetchData().then()
    }, []);

    const dataToRender = data.filter(({ code }) => localStor.includes(code));

    const list = dataToRender.map(item => (<ProductCard
        item={item}
        store={store}
        setStore={setStore}
        onClick={props.openFirst}
        key={item.code}/>));

    return (
        <div className="cards">

            {list}
        </div>
    )
};

export default Fav;
