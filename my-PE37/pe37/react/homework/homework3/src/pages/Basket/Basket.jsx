import React, {useEffect, useState} from 'react';
import  "./Basket.scss"
import ProductCard from '../../components/ProductCard/ProductCard'

const Basket = (props) => {

    const localStor = JSON.parse(localStorage.getItem("cardItems"));
    console.log(localStor.code);
    if(localStor) {

    }

    const list = localStor.map(item => (<ProductCard
        item={item}
        key={item.code}/>));


    return (
        <div className="basket">

            {list}

             </div>
    );
};

export default Basket;
