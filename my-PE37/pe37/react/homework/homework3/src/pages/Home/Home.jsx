import React, { useEffect, useState } from "react";

import Card from "../../components/Cards/index"
import Modal from "../../components/Modal/Modal";


const Home = () => {

    const [firstModal, setfirstModal] = useState(false);

    const [secondModal, setsecondModal] = useState(false);

    const [productToCart,setProductToCart] = useState({});

    const openFirst = () => {
        setfirstModal(true)
    };

    const closeFirst = ()=> setfirstModal(false);



    const openSecond = () => {
        setsecondModal(true)
    }
    const closeSecond = () => {
        setsecondModal(false)
    }


    useEffect(()=> {
        const favorite = localStorage.getItem("favoriteItem");
        const isFavorite = JSON.parse(favorite);
        if(!isFavorite){
            localStorage.setItem("favoriteItem", JSON.stringify([]))
        }
        const card = localStorage.getItem("cardItems");
        const isCard = JSON.parse(card);
        if(!isCard){
            localStorage.setItem("cardItems", JSON.stringify([]))
        }

    }, []);


    return (
        <div>

            <div className="App" onClick={event => event.stopPropagation()}>
                {firstModal && <Modal
                    ModalBox={{
                        backgroundColor: 'red',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                    setProductToCart={setProductToCart}
                    productToCart={productToCart}
                    closeModal={closeFirst}
                    active={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}
                    header='Do you want to delete this file?'
                    text='Once you delete this file, it won’t be possible to undo this action.
                            Are you sure you want to delete it?'
                    firstBtn='OK'
                    secondBtn='Cancel'
                />}
                <Card
                    openFirst={openFirst}
                    setProductToCart={setProductToCart}
                />
                {/*      <Button text={'Open second Modal'} onClick={this.openSecond} backgroundColor='blue'/>*/}
                {secondModal && <Modal
                    ModalBox={{
                        backgroundColor: 'blue',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                    closeModal={openSecond}
                    active={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}
                    header='Why did you press this button?'
                    text='Dont press this button again please'
                    firstBtn='OK'
                    secondBtn='Cancel'
                />}
            </div>

        </div>
    );
};

export default Home;
