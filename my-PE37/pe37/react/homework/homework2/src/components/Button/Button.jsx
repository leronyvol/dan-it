import React, {Component} from 'react';
import styles from './Button.scss'

class Button extends Component {


    render() {

        const { backgroundColor, text, onClick } = this.props
        return (
            <button className={styles.MyButton} style={{backgroundColor}} onClick={onClick}>
                {text}
            </button>
        );
    }

}

export default Button;