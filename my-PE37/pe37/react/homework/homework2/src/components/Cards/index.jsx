import React, {useState, useEffect} from 'react';
import ProductCard from "../ProductCard";
import styles from "./Card.scss"


const Cards = (props) => {

    const [data, setData] = useState([]);


    useEffect(() => {
        async function fetchData() {
            const fetchData = await fetch('./Product.json');
            const data = await fetchData.json();

            setData(data)


        }

        fetchData()
    }, []);
    const list = data.map(item => (<ProductCard
        item={item}
        onClick={props.openFirst}
        key={item.code}/>))

    return (

        <div className="cards">
            {list}
        </div>
    )


}
export default Cards;
