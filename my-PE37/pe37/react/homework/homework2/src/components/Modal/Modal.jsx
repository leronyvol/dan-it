import React, {Component} from 'react';
import styles from './Modal.scss'
import MyButton from "../Button/Button";


class Modal extends Component{

    render() {
        const { header, closeModal, text, firstBtn, secondBtn, active, ModalBox, headerBackground } = this.props



        return (
            <div className="Modal" style={active} onClick={e => (e.currentTarget === e.target) && closeModal()}>
                <div style={ModalBox} className="ModalBox">
                    <div className="ModalHeader" style={headerBackground}>
                        <h2>{header}</h2>
                        <span onClick={closeModal}>X</span>
                    </div>
                    <div className="text">{text}</div>
                    <footer>
                        <MyButton onClick={()=> localStorage.setItem("firstBtn",firstBtn)} text={firstBtn}/>
                        <MyButton  text={secondBtn}/>
                    </footer>
                </div>
            </div>
        );
    }

}

export default Modal;
