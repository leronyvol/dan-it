import React from "react";
class App extends React.Component {

    state = {
        title: 'Some title',
        user: {
            name: 'Stas',
            age: 27,
            hobby: 'football',
        }
    }

incAge = () => {
    console.log(this.state.age)
        this.setState((current) => ({
            user: {
                ...current.user,
                age: current.user.age + 1
            }
        })
        );

}

    updateTitle = () => {
        this.setState({
                title: 'Another title',
            }
        );

    }
    render() {
        const { title, user } = this.state;


        return (
            <>
                <div>
                    <p> { user.age}</p>
                    <p> {  user.name  }</p>
                    <p> { user.hobby ?  user.hobby : 'У Стаса нет хобби'   }</p>

            <h2>{ title }</h2>
                <button onClick={this.incAge}>Увеличить возраст</button>
                <button onClick={this.updateTitle}>Изменить заголовок</button>
                </div>
            </>

        );
    }
}


export default App;