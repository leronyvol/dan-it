import React, { Component } from "react";
import styles from './App.module.scss';

class App extends Component {
    // constructor() {
    //     super();
    //     this.state = {
    //
    //     }
    // }

    state = {
        title: 'Title from State',
        isImg: true
    }

    changeTitle = (event) => {
        console.log(event);
        this.setState({ title: 'Another title' })
    }

    hideImg = (text) => {
        console.log('text', text);
        this.setState((state) => {
            console.log('current: ', state);

            return {
                isImg: !state.isImg
            }
        })
    }

    render() {
        const { title, isImg } = this.state;

        return (
            <div className={styles.App}>
                <h1>{ title }</h1>
                { isImg && <img src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=1.00xw:0.669xh;0,0.190xh&resize=1200:*" alt=""/> }

                <button onClick={this.changeTitle}>Change title</button>
                <button onClick={() => this.hideImg('TEXT FROM FUNCTION')}>Hide image</button>
            </div>
        )
    }
}

export default App;