import {Component, useState, useEffect} from 'react';
import Header from "./components/Header";
import Footer from "./components/Footer";
import PostsContainer from "./components/PostsContainer";
import Preloader from "./components/Preloader";

const App = () =>{
    const [posts, setPosts] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [userId, setUserId] = useState(null)

       const  changeUserId =  (event)=> {
        setUserId(event.target.value )
    }

    const aa = async ()=>{
        setIsLoading(true)
        const posts = await fetch('https://ajax.test-danit.com/api/json/posts').then(res => res.json())
        setPosts(posts)
        setIsLoading(false)
    };

    useEffect( () => {
        (async ()=>{
            setIsLoading(true)
            const posts = await fetch('https://ajax.test-danit.com/api/json/posts').then(res => res.json())
            setPosts(posts)
            setIsLoading(false)
        })()

    },[]);

    useEffect(()=> async () => {
        if (userId) {
            setIsLoading(true)
            const posts = await fetch(`https://ajax.test-danit.com/api/json/posts?userId=${userId}`).then(res => res.json())
            setPosts( posts)
            setIsLoading(false)
        }
    },[userId])

    return(
                <div className="App">
            <Header title="PropTypes" userId={userId} changeUserId={changeUserId} user={{ name: 'Sam', age: 26,  avatar: 'https://i.pravatar.cc/40'}} />
            <PostsContainer posts={posts} isLoading={isLoading} />

            <Footer title="PropTypes" onOrderFunc={() => console.log('Order call')} />
        </div>

    )
}

// class App extends Component {
//     state = {
//         posts: [],
//         isLoading: false,
//         userId: null,
//     }
//
//     async componentDidMount() {
//         this.setState({isLoading: true})
//         const posts = await fetch('https://ajax.test-danit.com/api/json/posts').then(res => res.json())
//         this.setState({posts, isLoading: false})
//     }
//
//     async componentDidUpdate(){
//         const { userId } = this.state;
//
//         if (userId) {
//             this.setState({ isLoading: true, userId: null })
//             const posts = await fetch(`https://ajax.test-danit.com/api/json/posts?userId=${userId}`).then(res => res.json())
//             this.setState({ posts, isLoading: false})
//         }
//     }
//
//      changeUserId = async (event)=> {
//         this.setState({ userId: event.target.value })
//     }
//
//     render(){
//     const {posts, isLoading, userId} = this.state
//     return (
//         <div className="App">
//             <Header title="PropTypes" userId={userId} changeUserId={this.changeUserId} user={{ name: 'Sam', age: 26,  avatar: 'https://i.pravatar.cc/40'}} />
//             <PostsContainer posts={posts} isLoading={isLoading} />
//
//             <Footer title="PropTypes" onOrderFunc={() => console.log('Order call')} />
//         </div>
//     );
//   }
// }

export default App;
