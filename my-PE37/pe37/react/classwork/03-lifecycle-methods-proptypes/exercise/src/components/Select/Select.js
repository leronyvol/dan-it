import React, {PureComponent} from "react";

class Select extends PureComponent {


    render() {
        const { userId, changeUserId } = this.props;
        return (
            <>
                <select onChange={changeUserId} value={userId} name="select" id="select">
                    <option value={1}>1</option>
                    <option value={2}>2</option>
                    <option value={3}>3</option>
                    <option value={4}>4</option>
                </select>
            </>
        )
    }
}

export default Select

