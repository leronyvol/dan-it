import React, { PureComponent } from 'react';
import Button from "../Button";
import styles from './Footer.module.scss';
import PropTypes from 'prop-types';

class Footer extends PureComponent {
    render(){
        const { title, year, onOrderFunc } = this.props;

        return (
              <footer className={styles.root}>
                  <span>{title}</span>
                  <span>FE-30, {year}</span>
                  <Button type={"submit"} onClick={onOrderFunc}>Order Call</Button>
              </footer>
        );
    }




};
Footer.propTypes = {
    title: PropTypes.string,
    year: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    onOrderFunc: PropTypes.func
}
Footer.defaultProps = {
    title: '',
    year: new Date().getFullYear(),
    onOrderFunc: ()=>{}
}

export default Footer;
