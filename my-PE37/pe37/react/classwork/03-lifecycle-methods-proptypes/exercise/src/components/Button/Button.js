import React, {PureComponent} from 'react';
import classNames from "classnames";
import styles from './Button.module.scss';
import PropTypes from 'prop-types';

class Button extends PureComponent {
    render() {
        const {children, type, onClick} = this.props;

        return (
            <button className={classNames(styles.btn)} type={type} onClick={onClick}>{children}</button>
        );
    }
}

Button.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired,
    type: PropTypes.oneOf(['submit', 'button']),
    onClick: PropTypes.func,
}

Button.defaultProps = {
    type: 'button',
    onClick: ()=>{},
}

export default Button;
