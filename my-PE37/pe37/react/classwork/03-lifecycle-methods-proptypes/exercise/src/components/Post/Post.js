import React, { PureComponent } from 'react';
import styles from './PostsContainer.module.scss';

class Post extends PureComponent {
    render(){
        const { userId, title, body } = this.props;

        return (
            <div className={styles.root}>
                <span>User: {userId}</span>
                <h3>{title}</h3>
                <p>{body}</p>
            </div>
        );
    }
};


export default Post;
