import './App.css';
import AddNote from "./components/AddNote";
import NoteItem from "./components/NoteItem";
import NoteContainer from "./components/NoteContainer";

const notes = [
    {
        id: 0,
        text: 'JavaScript / React'
    },
    {
        id: 1,
        text: 'CSS / SASS (SCSS)'
    },
    {
        id: 2,
        text: 'HTML'
    },
    {
        id: 3,
        text: 'GIT'
    }
]

function App() {
  return (
    <div className="App">
        <AddNote />
        <NoteContainer notes={notes} />
    </div>
  );
}

export default App;
