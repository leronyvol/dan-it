import React from 'react';
import styles from './AddNote.module.scss';
import {Button, TextField} from "@mui/material";

const AddNote = () => {
    return (
        <>
            <div className={styles.root}>
                <TextField
                    type='text'
                    label='Your note'
                    className={styles.input}
                />
                <Button className={styles.btn} variant='contained'>Add Note</Button>
            </div>
            <div className={styles.line} />
        </>
)
}

export default AddNote;