import React from 'react';
import classNames from "classnames";
import PropTypes from 'prop-types';
import styles from './NoteItem.module.scss';
import {Button, Checkbox, TextField} from "@mui/material";
import { ReactComponent as EditSVG } from "../../assets/edit.svg";
import { ReactComponent as DeleteSVG } from "../../assets/delete.svg";
// import { ReactComponent as CheckSVG } from "../../assets/check.svg";

const NoteItem = (props) => {
    const { index, text } = props;
    const isDone = false;

    return (
        <li className={classNames(styles.root, { [styles.rootDone]: isDone })}>
            <div className={styles.wrapper}>
                <Checkbox className={styles.checkbox} />
                <span>{index}.</span>
                <p className={classNames({ [styles.done]: isDone })}>{text}</p>
            </div>

            <div className={styles.wrapper}>
                <Button variant='contained' className={styles.btn}><EditSVG /></Button>
                <Button variant='contained' color='error' className={styles.btn}><DeleteSVG /></Button>
            </div>
        </li>
    )
}

NoteItem.propTypes = {
    index: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    text: PropTypes.string,
};
NoteItem.defaultProps = {
    text: '',
};

export default NoteItem;


// <li className={styles.rootEdit}>
//     <TextField label='Edit Note' className={styles.input} />
//     <Button variant='contained' color='success' className={styles.btn} onClick={() => setIsEdited(false)}><CheckSVG /></Button>
// </li>