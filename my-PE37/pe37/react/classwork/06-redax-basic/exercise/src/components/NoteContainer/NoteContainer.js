import React from 'react';
import PropTypes from 'prop-types';
import NoteItem from "../NoteItem";

const NoteContainer = (props) => {
    const { notes } = props;

    if (!notes) return <p style={{ fontSize: 24 }}>You don't have any notes yet</p>;

    return (
        <ul>
            {notes && notes.map(({ text }, index) => <NoteItem index={index + 1} text={text} key={text} />)}
        </ul>
    )
}

NoteContainer.propTypes = {
    notes: PropTypes.array,
};
NoteContainer.defaultProps = {
    notes: null,
};

export default NoteContainer;