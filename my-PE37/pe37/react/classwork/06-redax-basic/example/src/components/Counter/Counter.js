import React from 'react';
import styles from './Counter.module.scss';

const Counter = () => {

    return (
        <div className={styles.root}>
            <p className={styles.counter}>0</p>
            <div className={styles.btnWrapper}>
                <button>+</button>
                <button>-</button>
                <button>Clear counter</button>
            </div>

            {/*<div className={styles.btnWrapper}>*/}
            {/*    <input type="text" />*/}
            {/*    <button>Set Value</button>*/}
            {/*</div>*/}
        </div>
    )
}

export default Counter;