import './App.css';
import Header from "./components/Header";
import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./Routes";


function App() {
  return (
          <Router>
            <div className="App">
                <Header />
                <section>
                    <Routes />
                </section>
            </div>
          </Router>
  );
}

export default App;
