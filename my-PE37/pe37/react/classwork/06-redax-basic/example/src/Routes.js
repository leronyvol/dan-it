import React from 'react';
import {Route, Switch} from "react-router-dom";
import HomePage from "./pages/HomePage";
import CounterPage from "./pages/CounterPage";


const Routes = () => (
        <Switch>
            <Route exact path='/' component={HomePage}/>
            <Route exact path='/counter' component={CounterPage} />
        </Switch>
);

Routes.propTypes = {};
Routes.defaultProps = {};

export default Routes;