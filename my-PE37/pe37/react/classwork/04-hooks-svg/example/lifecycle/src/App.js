import React, { useState } from 'react';
import getNameWithTime from "./utils/getNameWithTime";
import Image from "./components/Image";

const App = () => {
    const [counter, setCounter] = useState(0);
    const [src, setSrc] = useState(null);


    return (
        <div className="App">
            <h1>
                <a href="https://ru.reactjs.org/docs/react-component.html" target="_blank" rel="noreferrer">
                    Состояние и жизненный цикл
                </a>
            </h1>

            <p>{ counter }</p>
            <button onClick={() => setCounter(prev => prev + 1 )}>Increment counter</button>

            <div>
                <button onClick={() => setSrc(`https://picsum.photos/400/200?$versio=${Math.random()}`)}>Generate image src</button>
                <button onClick={() => setSrc(null)}>Delete image src</button>
            </div>

            {src && <Image counter={counter} src={src} />}
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
            <h1>d</h1>
        </div>
    );
}

// class App extends Component {
//
//     constructor() {
//         getNameWithTime('App', 'CONSTRUCTOR');
//         super();
//
//         this.state = {
//             counter: 0,
//             src: null,
//         }
//     }
//
//     componentDidMount() {
//         getNameWithTime('App','DID MOUNT');
//     }
//
//     componentDidUpdate() {
//         getNameWithTime('App','DID UPDATE');
//     }
//
//     componentDidCatch(error, errorInfo) {
//         console.log('COMPONENT DID CATCH')
//         console.log(error);
//         console.log(errorInfo);
//     }
//
//     static getDerivedStateFromError(error) {
//         console.log('GET DERIVED STATE FROM ERROR');
//         console.log(error);
//     }
//
//     render(){
//         const { counter, src } = this.state;
//           getNameWithTime('App','RENDER');
//           return (
//             <div className="App">
//                 <h1>
//                     <a href="https://ru.reactjs.org/docs/react-component.html" target="_blank" rel="noreferrer">
//                         Состояние и жизненный цикл
//                     </a>
//                 </h1>
//
//                 <p>{ counter }</p>
//                 <button onClick={() => this.setState(current => ({...current, counter: current.counter + 1}))}>Increment counter</button>
//
//                 <div>
//                     <button onClick={() => this.setState({ src: `https://picsum.photos/400/200?$versio=${Math.random()}` })}>Generate image src</button>
//                     <button onClick={() => this.setState({ src: null })}>Delete image src</button>
//                 </div>
//
//                 {src && <Image src={src} />}
//                 </div>
//         );
//     }
// }

export default App;
