import React, {useEffect, useState} from "react";
import ItemsContainer from "./components/ItemsContainer";
import Cart from "./components/Cart";
import styles from './App.module.scss';


function App() {
    const [items, setItems] = useState([])
    const [cartItem,setCartItem ] = useState([])

    const fetchItems = async () => {
        try {
            const res = await fetch('http://localhost:3001/items')
                .then(res  => res.json())
            setItems(res.data);
        } catch (error) {
            console.error(error);
        }


        // fetch('http://localhost:3001/items')
        //     .then(res  => res.json())
        //     .then(res => setItems(res.data));
    }

    useEffect(() => {
        fetchItems();
        }, [])

    const addToCart = (name,price) =>{
        console.log('name:',name ,'price:', price)
        setCartItem((prev) => {
            console.log(prev)
            const index = prev.findIndex(item => item.name === name)
            if(index === -1){
                return [...prev, { name, price, count: 1}];
            } else {
                const newState = [...prev];
                newState[index].count += 1
                return newState;

            }
        } )
    }


  return (
    <div className={styles.root}>
      <div>
        <ItemsContainer addToCart={addToCart} items={items} />
      </div>

      <div>
        <Cart items={cartItem} />
      </div>
    </div>
  );
}

export default App;
