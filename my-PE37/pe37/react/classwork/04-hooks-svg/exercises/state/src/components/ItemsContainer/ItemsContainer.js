import React from 'react';
import styles from './ItemsContainer.module.scss';
import Item from "../Item";

const ItemsContainer = (props) => {
    const { items, addToCart} = props;

    return (
        <section className={styles.root}>
            <h1>ITEMS</h1>
            <div className={styles.container}>
                {items && items.map(item => <Item key={item.name} {...item} addToCart={addToCart} />)}
            </div>
        </section>
    )
}

export default ItemsContainer;