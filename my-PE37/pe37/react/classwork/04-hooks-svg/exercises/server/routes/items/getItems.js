const items = require("../../bdMock/items");

const getItems = (app) => {
    app.get('/items', (req, res) => {
        return res.status(200).send(JSON.stringify({
            status: 'success',
            data: [
                ...items
            ],
        }))
    })
}

module.exports = getItems;
