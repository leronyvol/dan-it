const getItems = require('./getItems');
const updateItems = require('./updateItems');

module.exports = (app) => {
    getItems(app);
    updateItems(app);
}
