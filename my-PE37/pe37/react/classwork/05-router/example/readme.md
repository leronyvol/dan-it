### [BrowserRouter](https://v5.reactrouter.com/web/api/BrowserRouter)

`<Router>`, который использует [`API` истории `HTML5`](https://developer.mozilla.org/ru/docs/Web/API/History_API) (`pushState`, `replaceState` и событие `popstate`) для синхронизации вашего пользовательского интерфейса с URL-адресом.

```js

import { BrowserRouter as Router } from "react-router-dom";

<Router>
    <App />
</Router>

```

### [HashRouter](https://v5.reactrouter.com/web/api/HashRouter)

`<Router>`, который использует хеш-часть URL-адреса (например, `window.location.hash`) для синхронизации вашего пользовательского интерфейса с URL-адресом.

### [Link](https://v5.reactrouter.com/web/api/Link)
Предоставляет декларативную доступную навигацию по вашему приложению.

```js

import { Link } from "react-router-dom";

<Link to="/about">About</Link>


```

### [NavLink](https://v5.reactrouter.com/web/api/NavLink)

```js

import { Link } from "react-router-dom";

<NavLink to="/faq" className="aselected" activeClassName="selected--active">
  FAQs
</NavLink>

```

### [Route](https://v5.reactrouter.com/web/api/Route)

Компонент Route, пожалуй, самый важный компонент React Router, который нужно понять и научиться правильно использовать. Его основная задача - визуализировать некоторый пользовательский интерфейс, когда его путь совпадает с текущим URL-адресом.

```js

import { BrowserRouter as Router, Route } from "react-router-dom";

<Router>
    <Route exact path="/">
        <Home />
    </Route>
    <Route path="/news">
        <NewsFeed />
    </Route>
</Router>

```

[path](https://v5.reactrouter.com/web/api/Route/path-string-string)

Любой допустимый путь URL или массив путей, который понимает path-to-regexp@^1.7.0.


[exact: bool](https://v5.reactrouter.com/web/api/Route/exact-bool)

Когда true, будет соответствовать только в том случае, если путь в точности совпадает с location.pathname.

### Route render methods

[Route component](https://v5.reactrouter.com/web/api/Route/component)

Компонент React для рендеринга только при совпадении location. Он будет отрендерен с `route props`.

```js

<Router>
  <Route path="/user" component={User} />
</Router>

```

[render: func](https://v5.reactrouter.com/web/api/Route/render-func)

Вместо создания нового элемента React, созданного для вас с помощью свойства `component`, вы можете передать функцию, которая будет вызываться при совпадении местоположения. Эта функция будет иметь доступ к пропсам роутера так же как и `component`

```js

<Route render={routeProps => (
          <Component {...routeProps} />
      )}
/>

```

### [Switch](https://v5.reactrouter.com/web/api/Switch)

Отображает первый дочерний элемент <Route> или <Redirect>, который соответствует местоположению.

```js

<Switch>
    <Route exact path="/">
      <Home />
    </Route>
    <Route path="/about">
      <About />
    </Route>
    <Route path="/:user">
      <User />
    </Route>
    <Route>
      <NoMatch />
    </Route>
</Switch>

```

### [Redirect](https://v5.reactrouter.com/web/api/Redirect)

Rendering a <Redirect> will navigate to a new location. 

```js

<Route exact path="/">
  {loggedIn ? <Redirect to="/dashboard" /> : <PublicHomePage />}
</Route>

```

# Архитектура роутинга
