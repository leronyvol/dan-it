import React from 'react';

const Test = (props) => {
    console.log(props);
    const { history } = props;

    return (
        <>
            <p>TEST TEST</p>
            <button onClick={() => history.push('/test')}> GO TO TEST</button>
        </>
    )
}


export default Test;