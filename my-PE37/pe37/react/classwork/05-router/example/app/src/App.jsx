import './App.css';
import Header from "./components/Header";
import Routes from "./Routes";
import {BrowserRouter as Router,  NavLink} from "react-router-dom";

function App() {
    return (
        <Router>
            <div className="App">
                <Header/>
        <Routes />


            </div>
        </Router>
    );
}

export default App;
