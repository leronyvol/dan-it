import React from 'react';
import PropTypes from "prop-types";
import styles from './Header.module.scss';

const Header= (props) => {
        const { title, user: { name, company } } = props;

        return (
              <header className={styles.root}>
                  <span>{title}</span>
                  <nav>
                      <ul>
                          <li>
                              <a href="">link</a>
                          </li>
                          <li>
                              <a href="">link</a>
                          </li>
                          <li>
                              <a href="">link</a>
                          </li>
                          <li>
                              <a href="">link</a>
                          </li>
                      </ul>
                  </nav>
                  <div className={styles.userContainer}>
                      <span>{name}, {company}</span>
                  </div>
              </header>
        );
};

export default Header;
