import { Component } from 'react';
import Header from "./components/Header";
import Footer from "./components/Footer";
import './App.scss';
import HomePage from './pages/HomePage'


class App extends Component {

    render(){
        return (
            <div className="App">
                <Header title={123} user={{ name: 'Sam', company: 'Some Company' }} />
                <HomePage />
                <Footer title="PropTypes" year={new Date().getFullYear()} onOrderFunc={() => console.log('Order call')} />
            </div>
        );
    }
}

export default App;
