import React from "react";
import Button from "../../components/Button";

const HomePage = () => {

    return (
        <section>
            <h1 >HOME</h1>
            <Button>Go to the Posts</Button>
            <Button>Go to the Users</Button>
        </section>
    )
}

export default HomePage;