import React from 'react';
import PropTypes from "prop-types";
import styles from './Button.module.scss';

const Button = (props) => {
        const { children, type, onClick } = props;

        return (
            <button className={styles.btn} type={type} onClick={onClick}>{children}</button>
        );
};

Button.propTypes = {
        children: PropTypes.oneOfType([
                PropTypes.arrayOf(PropTypes.node),
                PropTypes.node
        ]).isRequired,
        type: PropTypes.oneOf(['submit', 'button', 'reset']),
        onClick: PropTypes.func,
}

Button.defaultProps = {
        type: 'button',
        onClick: ()=>{},
}

export default Button;
