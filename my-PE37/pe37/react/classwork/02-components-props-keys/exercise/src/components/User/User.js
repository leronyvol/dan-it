import React, {PureComponent} from "react";
import styles from "./User.module.scss"
class User extends PureComponent{
    render(){
        const{avatar,name} =this.props
        return (<div className={styles.headerUserContainer}>
            <img src={avatar} alt={name} />
            <span>{name}</span>
        </div>)
    }
}
export default User