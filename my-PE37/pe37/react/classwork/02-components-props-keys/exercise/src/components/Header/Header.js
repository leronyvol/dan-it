import React, {PureComponent} from "react";
import User from "../User/User";
import styles from "./Header.module.scss"
class Header extends PureComponent{
    render(){
        const {title,user, actions} = this.props;
        console.log(user);

        return <header>
            <span className={styles.headerTitle}>{title}</span>

            <nav>
                <ul>
                    <li>
                        <a href='/home'>Home</a>
                    </li>

                    <li>
                        <a href='/blog'>Blog</a>
                    </li>
                </ul>
            </nav>
            {actions}
        <User avatar={user.avatar} name={user.name}/>

        </header>
    }
}
export default Header