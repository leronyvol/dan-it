import './App.css';
import React, { Component } from 'react';
import ListItem from "./components/ListItem/ListItem";

class App extends Component {
  state = {
    items: [
        '0.9725067665779998',
        '0.5223571231487212',
        '0.8613543409506943',
        '0.49496115828960896',
        '0.31147916020306465',
        '0.9118810877216494',
    ],
  }

  updateState = () => {
      this.setState(state => ({items: [`${Math.random()}`, ...state.items]}))
      // this.setState(state => ({ items: [...state.items, `${Math.random()}`] })) // Все +- ОК пока мы добавляем элементы в конец списка
  }

  render(){
    const { items } = this.state;

    return (
        <div className="App">
          <div style={{ padding: '40px 10px' }}>
              <button onClick={this.updateState}>Update state</button>
            <ul>
                {items.map(item => <ListItem key={item}>{item}</ListItem>)}
            </ul>
          </div>
        </div>
    );
  }

}

export default App;
