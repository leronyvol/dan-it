import React, {Component} from "react";
import './App.css';
import Button from "./components/Button";
import Container from "./components/Container/Container";

const data = [
    {
        text: {
            id: '1'
        },
    }, {
        text: {
            id: '1'
        },
    }, {
        text: {
            id: '1'
        },
    }, {
        text: {
            id: '1'
        },
    },
];


class App extends Component {
    state = {
        title: 'Some title',
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores consequatur numquam quibusdam!',
    }

    changeTitle = (newTitle) => {
        this.setState(({ title: newTitle }))
    }


    render() {
        const {title, text} = this.state;

        return (
            <div className="App">
                <h1>{title}</h1>

                <Button handleClick={() => this.setState({ title: 'Another title' })}>Change title</Button>
                <Button handleClick={() => this.setState({ text: 'Another text' })}>Change text</Button>

                {
                    data.map(({ text: { id } }) => <Button>{ id }</Button>)
                }
                <Container handleClick={this.changeTitle} text={text}/>
            </div>
        )
    }
}

export default App;
