const defaultClasses = {
    container: "randomizer",
    start: ["randomizer__btn", "randomizer__btn--start"],
    reset: ["randomizer__btn", "randomizer__btn--reset"],
    exclude: ["randomizer__btn", "randomizer__btn--exclude"],
    title: "randomizer__title",
};

function Randomizer({
                        items,
                        storageName = "items",
                        title,
                        parent = document.body,
                        classes = {},
                        exclusion = true
                    }) {
    this.elements = {
        start: document.createElement("button"),
        reset: document.createElement("button"),
        exclude: document.createElement("button"),
        title: document.createElement("h3"),
        container: document.createElement("div"),
    };
    this.exclusion = exclusion;
    this.classes = {...defaultClasses, ...classes};
    this.parent = parent;
    this.titleText = title;
    this.storageName = storageName;
    this.items = JSON.parse(localStorage.getItem(this.storageName)) || items;
    this.wasSelected =
        JSON.parse(localStorage.getItem(`selected_${this.storageName}`)) || [];

    this.render();
}

Randomizer.prototype.start = function () {
    let {
        storageName,
        items,
        wasSelected,
        elements: {title, container, exclude},
    } = this;

    if (items.length > 0) {
        wasSelected.push(
            items.splice(
                Math.floor(Math.round(Math.random() * items.length - 1)),
                1
            )[0]
        );
        items = items.sort(() => Math.random() - 0.5);
        title.textContent = wasSelected[wasSelected.length - 1];
        title.style.color = '#0f0f0f';
        if (this.exclusion && ![...container.children].some(el => el === exclude)) {
            container.append(exclude)
        }
    } else {
        this.items = wasSelected.sort((el) => Math.random() - 0.5);
        this.wasSelected = [];
        this.fillTheStore(items, wasSelected);
        title.textContent = "That's all :D";
        title.style.color = '#ff0000';
    }

    this.fillTheStore(items, wasSelected);
};

Randomizer.prototype.fillTheStore = function (items, wasSelected) {
    localStorage.setItem(
        `selected_${this.storageName}`,
        JSON.stringify(wasSelected)
    );
    localStorage.setItem(this.storageName, JSON.stringify(items));
};

Randomizer.prototype.handleReset = function () {
    const {storageName, titleText} = this;
    const {title} = this.elements;
    localStorage.removeItem(storageName);
    localStorage.removeItem(`selected_${storageName}`);
    this.wasSelected = [];
    title.textContent = titleText;
    window.location.reload();
};

Randomizer.prototype.handleStartClick = function () {
        this.start();
};

Randomizer.prototype.handleExclude = function () {
    const {storageName} = this;
    const {title} = this.elements;
    const storageItems = JSON.parse(localStorage.getItem(storageName));
    const storageSelectedItems = JSON.parse(localStorage.getItem(`selected_${storageName}`));
    const compareItems = el => el.toLowerCase() === title.textContent.toLowerCase();
    const itemToExclude = storageItems.find(compareItems)
        || storageSelectedItems.find(compareItems);

    if (!storageItems || !itemToExclude) {
        throw new Error("No any items to exclude")
    }

    if (storageItems.indexOf(itemToExclude) >= 0) {
        storageItems.splice(
            storageItems.indexOf(itemToExclude),
            1
        );
    }

    if (storageSelectedItems.indexOf(itemToExclude) >= 0) {
        storageSelectedItems.splice(
            storageSelectedItems.indexOf(itemToExclude),
            1
        );
    }

    this.items = storageItems;
    this.wasSelected = storageSelectedItems;

    this.fillTheStore(storageItems, storageSelectedItems);
    this.handleStartClick();
};

Randomizer.prototype.render = function () {
    const {
        elements: {start, reset, title, container, exclude},
        parent,
        titleText,
    } = this;
    const {
        container: classContainer,
        title: classTitle,
        reset: classReset,
        start: classStart,
        exclude: classExclude,
    } = this.classes;

    title.textContent = titleText;
    start.innerHTML = `<img src="./assets/target-account.svg"/><span> Make a choice </span>`
    reset.innerHTML = `<img src="./assets/restart.svg"/>`;
    exclude.innerHTML = `<img src="./assets/account-remove.svg"/>`;

    container.classList.add(classContainer);
    title.classList.add(classTitle);
    start.classList.add(...classStart);
    reset.classList.add(...classReset);

    this.elements.start.addEventListener("click", () => this.handleStartClick());
    this.elements.reset.addEventListener("click", () => this.handleReset());

    container.append(title, start, reset);

    if (this.exclusion) {
        exclude.classList.add(...classExclude);
        this.elements.exclude.addEventListener("click", () => this.handleExclude());
    }
    parent.insertAdjacentElement("beforeend", container);
};
