const gulp = require('gulp'),
	concat = require('gulp-concat'),
	clean = require('gulp-clean'),
	browserSync = require('browser-sync').create(),
	sass = require('gulp-sass');


/*** PATHS ***/
const paths = {
	src: {
		scss: './src/scss/**/*.scss',
		js: './src/js/*.js',
	},
	build: {
		css: './build/css/',
		js: './build/js/',
		self: './build/',
	},
}

/*** FUNCTIONS ***/

const buildJS = () =>
	gulp
		.src(paths.src.js)
		.pipe(concat('script.js'))
		.pipe(uglify())
		.pipe(minifyjs())
		.pipe(gulp.dest(paths.build.js))
		.pipe(browserSync.stream())

const buildCSS = () =>
	gulp
		.src(paths.src.scss)
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest(paths.build.css))
		.pipe(browserSync.stream())

const cleanBuild = () => gulp.src(paths.build.self, { allowEmpty: true }).pipe(clean())

const watcher = () => {
	browserSync.init({
		server: {
			baseDir: './',
		},
	})

	gulp.watch(paths.src.scss, buildCSS).on('change', browserSync.reload)
	gulp.watch(paths.src.js, buildJS).on('change', browserSync.reload)
}

/*** TASKS ***/
gulp.task('clean', cleanBuild)
gulp.task('buildCSS', buildCSS)
gulp.task('buildJS', buildJS)

gulp.task('dev', gulp.series(cleanBuild, buildCSS, buildJS, watcher))
gulp.task('build', gulp.series(cleanBuild, buildCSS, buildJS, watcher))
